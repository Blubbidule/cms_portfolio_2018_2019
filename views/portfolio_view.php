<?php

function showStartHtml(){
 echo '<html>
    <head>
      <meta charset="utf-8" />
	<title>CV Delaunois Thibault</title>
      <link href="http://localhost/cms_portfolio_2018_2019/style.css" rel="stylesheet" />
    </head>
    <body>
        <header>
	 
	   <div class="col col-1-12"><p> </p></div>
	   
	   <div class="col col-1-12">
	     <a href="http://localhost/cms_portfolio_2018_2019?page=accueil"><img class="logoperso" src="http://localhost/cms_portfolio_2018_2019/img/logo.png" alt="logo" /></a>
	   </div><!--logo du site, ramène à l\'acceuil-->
	   

';
}


//Fonctions pour affichage

//Affichage liens mis en avant
function ShowLinksAccueil(){
   echo' <div class="col col-3-12"><p> </p></div> 
       <div class="col col-2-12">
	     <h1 class="hometitle">PORTFOLIO</h1><!-- titre de la page visible sur la page-->
	   </div>
	   <div class="col col-5-12 last"><p> </p></div>
	   <div class="clear"></div>
	   
	   <div class="col col-4-12"><p> </p></div>
	   
	   <div class="col col-5-12">
       <nav>
		   <ul class="headerlink">
		    <li><a class="actualpage" href="http://localhost/cms_portfolio_2018_2019?page=accueil">Accueil</a></li>
		    <li><a href="http://localhost/cms_portfolio_2018_2019?page=services">Services proposés</a></li>
                    <li><a href="http://localhost/cms_portfolio_2018_2019?page=works">Travaux réalisés</a></li>
                    <li><a href="http://localhost/cms_portfolio_2018_2019?page=contact">Contact</a></li>
		   </ul>
		 </nav>
	   </div>
	   
	   <div class="col col-3-12"><p> </p></div>
	   <div class="clear"></div>
	   
        </header>
        <div class="mainbody">';
}

function ShowLinksServices(){
   echo' <div class="col col-2-12"><p> </p></div> 
       <div class="col col-4-12">
	     <h1 class="servicetitle">Services proposés</h1><!-- titre de la page visible sur la page-->
	   </div>
	   <div class="col col-4-12 last"><p> </p></div>
	   <div class="clear"></div>
	   
	   <div class="col col-4-12"><p> </p></div>
	   
	   <div class="col col-5-12">
       <nav>
		   <ul class="headerlink">
		    <li><a href="http://localhost/cms_portfolio_2018_2019?page=accueil">Accueil</a></li>
		    <li><a class="actualpage" href="http://localhost/cms_portfolio_2018_2019?page=services">Services proposés</a></li>
                    <li><a href="http://localhost/cms_portfolio_2018_2019?page=works">Travaux réalisés</a></li>
                    <li><a href="http://localhost/cms_portfolio_2018_2019?page=contact">Contact</a></li>
		   </ul>
		 </nav>
	   </div>
	   
	   <div class="col col-3-12"><p> </p></div>
	   <div class="clear"></div>
	   
        </header>
        <div class="mainbody">';
}

function ShowLinksWorks(){
   echo'  <div class="col col-2-12"><p> </p></div> 
       <div class="col col-4-12">
	     <h1 class="worktitle">Travaux réalisés</h1><!-- titre de la page visible sur la page-->
	   </div>
	   <div class="col col-4-12 last"><p> </p></div>
	   <div class="clear"></div>
	   
	   <div class="col col-4-12"><p> </p></div>
	   
	   <div class="col col-5-12">
       <nav>
		   <ul class="headerlink">
		    <li><a href="http://localhost/cms_portfolio_2018_2019?page=accueil">Accueil</a></li>
		    <li><a href="http://localhost/cms_portfolio_2018_2019?page=services">Services proposés</a></li>
                    <li><a class="actualpage" href="http://localhost/cms_portfolio_2018_2019?page=works">Travaux réalisés</a></li>
                    <li><a href="http://localhost/cms_portfolio_2018_2019?page=contact">Contact</a></li>
		   </ul>
		 </nav>
	   </div>
	   
	   <div class="col col-3-12"><p> </p></div>
	   <div class="clear"></div>
	   
        </header>
        <div class="mainbody">
         <div class="col col-3-12"><p> </p></div>
	   
	   <div class="col col-7-12">
	     <h2>Voici quelques travaux réalisés jusqu\'à présent :</h2>
	   </div>
	   
	   <div class="col col-2-12 last"><p> </p></div>
	   <div class="clear"></div>';
}

function ShowLinksContact(){
   echo' <div class="col col-3-12"><p> </p></div>
       <div class="col col-2-12">
	     <h1 class="touchtitle">Contact</h1><!-- titre de la page visible sur la page-->
	   </div>
	   <div class="col col-5-12 last"><p> </p></div>
	   <div class="clear"></div>
	   
	   <div class="col col-4-12"><p> </p></div>
	   
	   <div class="col col-5-12">
       <nav>
		   <ul class="headerlink">
		    <li><a href="http://localhost/cms_portfolio_2018_2019?page=accueil">Accueil</a></li>
		    <li><a href="http://localhost/cms_portfolio_2018_2019?page=services">Services proposés</a></li>
                    <li><a href="http://localhost/cms_portfolio_2018_2019?page=works">Travaux réalisés</a></li>
                    <li><a class="actualpage" href="http://localhost/cms_portfolio_2018_2019?page=contact">Contact</a></li>
		   </ul>
		 </nav>
	   </div>
	   
	   <div class="col col-3-12"><p> </p></div>
	   <div class="clear"></div>
	   
        </header>
        <div class="mainbody">';
}


//Affichage du contenu des pages
//
//Fonction affichage Accueil
function showAccueilPageContent($accueil) { 
    $accueilcontent = '<div class="centerText">';
        foreach ($accueil as $line) {
            $accueilcontent .= '<h2>'.$line['titre_accueil'].'</h2>';
            $accueilcontent .= '<p>'.$line['texte_accueil'].'</p>';
        }   
            $accueilcontent .= '</div>';
            
    return $accueilcontent;
}

//Fonction affichage Services proposés
function showServicesPageText($servicestext) {
    $servicetextcontent = '<div class="centertext">';
        
            $servicetextcontent .= '<p>'.$servicestext['service_expl'].'</p>';
            $servicetextcontent .= '</div>';
            
    return $servicetextcontent;
}

function showServicesPageImg($servicesimg) {
    $serviceimgcontent = '<div class="col col-4-12"><p> </p></div>';
    foreach ($servicesimg as $line){
        
        $serviceimgcontent .= '<div class="col col-4-12"><img src="'.$line['services_images'].'" alt="service_image_'.$line['id'].'" /></div>';  
        $serviceimgcontent .= '<div class="col col-4-12 last"><p> </p></div>';
        
    }
    $serviceimgcontent .= '<div class="clear"></div>';
    return $serviceimgcontent;
}

//Fonction affichage Travaux réalisés
function showWorksContent($workscontent) {
    
     $allworkscontent = '<div class="col col-4-12"><p> </p></div>';
    foreach ($workscontent as $line){
        if ($line['images_travaux'] !== null){
        $allworkscontent .= '<div class="col col-4-12"><h3 class="centertext">'.$line['titre_travaux'].'</h3></div>';  
//        $allworkscontent .= '<div class="col col-1-12 last"><p> </p></div>';
//        $allworkscontent .= '<div class="col col-3-12"><p>'.$line['titre_travaux'].'</p></div>'; 
        $allworkscontent .= '<div class="col col-4-12 last"><p> </p></div>';
        $allworkscontent .= '<div class="clear"></div>';
        
        $allworkscontent .= '<div class="col col-4-12"><p> </p></div>';
        $allworkscontent .= '<div class="col col-4-12"><img src="'.$line['images_travaux'].'" alt="image_travail_'.$line['id'].'" /></div>';  
//        $allworkscontent .= '<div class="col col-1-12 last"><p> </p></div>';
//        $allworkscontent .= '<div class="col col-3-12"><img src="'.$line['images_travaux'].'" alt="image_travail_'.$line['id'].'" /></div>'; 
        $allworkscontent .= '<div class="col col-4-12 last"><p> </p></div>';
        }
    }
    $allworkscontent .= '<div class="clear"></div>';
    return $allworkscontent;
}

//Fonction affichage Contact
function showContactPersonalData($personaldata) {
    $contactPersonalData = '<div class="col col-5-12"><p> </p></div>
	   <div class="col col-3-12">
	     <h2>Me contacter:</h2>
	   </div>
	   <div class="col col-4-12 last"><p> </p></div>
	   <div class="clear"></div>
            <div class="col col-1-12"><p> </p></div>
                 <div class="col col-10-12 padtext contacttext">
                     <p>Pour me contacter directement et avec plus de précisions, utilisez l\'un des moyens ci-dessous:<br/><br/>
                        <span id="addressperso"> ';
       
            $contactPersonalData .= 'Téléphone : '.$personaldata['telephone'].'<br/><br/>';
            $contactPersonalData .= 'Adresse mail : '.$personaldata['mail_perso'].'<br/><br/>';
            $contactPersonalData .= 'Courrier : '.$personaldata['adresse'].'<br/>';
            $contactPersonalData .= '</span> 
                        <br/>
                        Je réponds aussi vite que possible aux questions que l\'on me pose, et je m\'efforce d\'être le plus complet possible dans mes réponses.<br/>
			<br/>
			N\'hésitez pas à me poser toutes les questions que vous pourriez avoir sur mon portfolio, sur moi-même ou mes disponibilités.<br/>
			<br/>
			<span id="adjust">Je me ferais un plaisir d\'y répondre.</span>
                     </p><!--fin du texte de contact-->
                 </div>
         
	   <div class="col col-1-12 last"><p> </p></div>
	   <div class="clear"></div>';
            
    return $contactPersonalData;
}

function showContactForm() {
   return  '<div class="col col-1-12"><p> </p></div>
       <div class="col col-3-12">
	   <form action="" id="contact_form" method="post" name="contact_form">
	  <ul>
      <li><label>NOM <span class="required">*</span></label><input type="text" name="nom" id="name" value="" class="requiredField"/></li>
	  
      <li><label>E-MAIL <span class="required">*</span></label><input type="text" name="email" id="email" value="" class="requiredField email"/></li>
	  
	  <li class="textarea"><label>MESSAGE <span class="required">*</span></label><textarea name="message" id="message" rows="10" cols="30" class="requiredField" ></textarea></li>
	  
      <li class="button_form"><input name="submitted" id="submitted" class="submit" type="submit" value="Envoyer" /><input id="reset" type="reset" value="Effacer" class="submit" /></li>
      </ul>
	  </form><!--fin du formulaire pour lenvoie de message-->
	 </div>
	   
	   <div class="col col-1-12"><p> </p></div>'; 
}

function showContactMap($googlemap) {

        $positionmap = '<div class="col col-6-12">'; 
	$positionmap .= '<iframe width="640" height="345" class="goomap" src="'.$googlemap['map'].'"  style="border:0" allowfullscreen></iframe><!--map google de mon adresse-->';
	$positionmap .=' </div> 
         <div class="col col-1-12 last"><p> </p></div>
	 <div class="clear"></div>';
    
        return $positionmap;
}


//Fonction pour afficher footer et fin du document
function showEndHtml() {
    	echo '</div>
	 
	 <footer>
	   <div class="col col-7-12"><p> </p></div>
	   
	   <div class="col col-4-12">
	     <div class="copyright">
	       &copy; 2019 Delaunois Thibault <a href="http://www.seraingsup.be/">IPEPS Seraing Sup</a>
	     </div><!--droit d\'auteur du site-->
	   </div>
	   
	   <div class="col col-1-12 last"><p> </p></div>
	   <div class="clear"></div>
	   
	 </footer>
   
   </body>
   
 </html>';
}