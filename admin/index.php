<?php
session_start();
include 'model/admin_model.php';
include 'views/admin_view.php';


//if (tokenExists()){
//    displayContent();
//} else {
//    if (isLoginFormSubmitted()){
//        if (isPassAndUserValid()){
//            defineToken();
//            displayContent();
//        }
//        else{
//            displayError();
//            displayForm();
//        }
//    }else{
//        displayForm();
//    }
//}
//
//
//function defineToken(): void {
//    if (isRememberMe()){
//        setcookie('token',"1",time() + 30 * 24 *60 * 60,"","",false,true);
//    }
//    else{
//        $_SESSION['token'] = 1;
//    }
//}
//
//function isRememberMe(){
//    return isset($_POST['rememberMe']) && $_POST['rememberMe'] === "1";
//}
//
//
//function tokenExists() :bool {
//    return isset($_SESSION['token']) || isset($_COOKIE['token']);
//}
//
//
//function isLoginFormSubmitted(){
//    if (isset($_POST['username'])&& isset($_POST['password'])){
//        return true;
//    }
//}
//
//function isPassAndUserValid(){
//    $db = getPdo();
//
//    $username = 'Admin';
//    $password = 'Bienvenue';
//    
//    $_POST['username'] = htmlspecialchars($_POST['username']);
//    $_POST['password'] = htmlspecialchars($_POST['password']);
//    
//    if($_POST['username'] === $username && $_POST['password'] === $password){
//        return true;
//    }
//}
//function displayError(){
//    echo '<p style="color:red;">Les données entrées son incorrectes</p>';
//}
//function displayContent(){
//     
//}
//function displayForm(){
//    echo '<form action="adminPage.php" method="post">
//
//        <label for="username">username</label>
//        <input type="text" name="username" placeholder="username">
//        
//        <label for="password">password</label>
//        <input type="password" name="password" placeholder="password">
//        
//        <label for="remember_me">Remember me</label>
//        <input type="checkbox" id="remember_me" name="rememberMe" value="1">
//
//        <input type="submit" >
//    </form>';
//}

if (tokenExists()) {
    displayContent();
}
else{
    if (isFormSubmitted()){
        if (isFormValid()){
            defineToken();
            reudirect();
        }else{
            displayError();
            displayForm();
        }
    }
    else{
        displayForm();
    }
}


function reudirect(){
    header('Location: index.php');
}

function defineToken(): void {
    if (isRememberMe()){
        setcookie('token',"1",time() + 30 * 24 *60 * 60,"","",false,true);
    }
    else{
        $_SESSION['token'] = 1;
    }
}

function isRememberMe(){
    return isset($_POST['rememberMe']) && $_POST['rememberMe'] === "1";
}


function tokenExists() :bool {
    return isset($_SESSION['token']) || isset($_COOKIE['token']);
}

function isFormValid(): bool {
    $db = getPdo();
    $login = 'Admin';
    $pass = 'Mauvaise réponse';
    
    $_POST['login'] = htmlspecialchars($_POST['login']);
    $_POST['password'] = htmlspecialchars($_POST['password']);

    return $_POST['login'] === $login && $_POST['password'] === $pass;
}

function isFormSubmitted(): bool{
    return isset($_POST['login']) && isset($_POST['password']);
    // equivalent à if (isset($_POST['login']) && isset($_POST['password'])) {return true;} else{ return false;}
}

function displayError() {
    echo '<p>Identifiants incorrects</p>';
}

function displayContent() {
    header('Location: adminPage.php');
}


function displayForm(){
    echo '<form action="index.php" method="post">
    <div>
        <label for="login">username</label>
        <input type="text" id="login" name="login">
    </div>
    <div>
        <label for="pass">password</label>
        <input type="password" id="pass" name="password">
    </div>
    <div>
        <label for="remember_me">Remember me</label>
        <input type="checkbox" id="remember_me" name="rememberMe" value="1">
    </div>
    <button>submit</button>
</form>
';
}


