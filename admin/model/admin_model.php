<?php


function getPdo() {
    try {
        $db = new PDO('mysql:host=localhost;dbname=cms_portfolio;charset=utf8', 'root', 'root');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        die('connection error : ' . $e->getMessage());
    }

    return $db;
}

// Fonction pour table accueil

function insertIntoAccueil($titre_accueil, $texte_accueil, $photo_presentation) {
    $db = getPdo();
    try {
        $request = 'INSERT INTO `accueil` (`titre_accueil`,`texte_accueil`, `photo_presentation`) VALUES (:titre_accueil, :texte_accueil, :photo_presentation)';
        $insert = $db->prepare($request);
        $insert->execute(['titre_accueil' => $titre_accueil, 'texte_accueil' => $texte_accueil, 'photo_presentation' => $photo_presentation]);
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function deleteIntoAccueil($id) {
    $db = getPdo();
    //require_once("index.php");
    $where = ['id' => $id];
    $db->prepare("DELETE FROM `accueil` WHERE id=:id")->execute($where);
   
}

function updateAccueil($id, $titre_accueil, $texte_accueil, $photo_presentation) {
    $db = getPdo();
    try {
        $request = "UPDATE `accueil` SET titre_accueil=:titre_accueil, texte_accueil=:texte_accueil, photo_presentation=:photo_presentation WHERE id=:id";
        $update = $db->prepare($request);
        $update->execute(['titre_accueil' => $titre_accueil, 'texte_accueil' => $texte_accueil, 'photo_presentation' => $photo_presentation, 'id' => $id] );
    } catch (PDOExecption $e) {
        die($e->getMessage());
    }
}

function findOneIntoAccueil($id){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `accueil` WHERE `id`= :id');
        $reponse->execute(['id'=>$id]);
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

function findAllIntoAccueil(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `accueil`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

// Fonction pour table Services

function insertIntoServices($service_expl, $service_images) {
    $db = getPdo();
    try {
        $request = 'INSERT INTO `services_proposes` (`service_expl`,`services_images`) VALUES (:service_expl, :services_images)';
        $insert = $db->prepare($request);
        $insert->execute(['service_expl' => $service_expl, 'services_images' => $service_images]);
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function deleteIntoServices($id) {
    $db = getPdo();
    //require_once("index.php");
    $where = ['id' => $id];
    $db->prepare("DELETE FROM `services_proposes` WHERE id=:id")->execute($where);
   
}

function updateServices($id, $service_expl, $service_images) {
    $db = getPdo();
    try {
        $request = "UPDATE `services_proposes` SET service_expl=:service_expl, services_images=:services_images WHERE id=:id";
        $update = $db->prepare($request);
        $update->execute(['service_expl' => $service_expl, 'services_images' => $service_images, 'id' => $id] );
    } catch (PDOExecption $e) {
        die($e->getMessage());
    }
}

function findOneIntoServices($id){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `services_proposes` WHERE `id`= :id');
        $reponse->execute(['id'=>$id]);
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

function findAllIntoServices(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `services_proposes`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

// Fonction pour table contact

function insertIntoContact($map, $nom, $mail, $message) {
    $db = getPdo();
    try {
        $request = 'INSERT INTO `contact` (`map`,`nom`, `email`, `message`) VALUES (:map, :nom, :email, :message)';
        $insert = $db->prepare($request);
        $insert->execute(['map' => $map, 'nom' => $nom, 'email' => $mail, 'message' =>$message]);
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function deleteIntoContact($id) {
    $db = getPdo();
    //require_once("index.php");
    $where = ['id' => $id];
    $db->prepare("DELETE FROM `contact` WHERE id=:id")->execute($where);
   
}

function updateContact($id, $map, $nom, $mail, $message) {
    $db = getPdo();
    try {
        $request = "UPDATE `contact` SET map=:map, nom=:nom, email=:email, message=:message WHERE id=:id";
        $update = $db->prepare($request);
        $update->execute(['map' => $map, 'nom' => $nom, 'email' => $mail, 'message'=>$message, 'id' => $id] );
    } catch (PDOExecption $e) {
        die($e->getMessage());
    }
}

function findOneIntoContact($id){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `contact` WHERE `id`= :id');
        $reponse->execute(['id'=>$id]);
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

function findAllIntoContact(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `contact`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

// Fonction pour table travaux

function insertIntoWorks($titre_travaux, $images_travaux, $video_travaux) {
    $db = getPdo();
    try {
        $request = 'INSERT INTO `travaux_realises` (`titre_travaux`,`images_travaux`, `video_travaux`) VALUES (:titre_travaux, :images_travaux, :video_travaux)';
        $insert = $db->prepare($request);
        $insert->execute(['titre_travaux' => $titre_travaux, 'images_travaux' => $images_travaux, 'video_travaux' => $video_travaux]);
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function deleteIntoWorks($id) {
    $db = getPdo();
    //require_once("index.php");
    $where = ['id' => $id];
    $db->prepare("DELETE FROM `travaux_realises` WHERE id=:id")->execute($where);
   
}

function updateWorks($id, $titre_travaux, $images_travaux, $video_travaux) {
    $db = getPdo();
    try {
        $request = "UPDATE `travaux_realises` SET titre_travaux=:titre_travaux, images_travaux=:images_travaux, video_travaux=:video_travaux WHERE id=:id";
        $update = $db->prepare($request);
        $update->execute(['titre_travaux' => $titre_travaux, 'images_travaux' => $images_travaux, 'video_travaux' => $video_travaux, 'id' => $id] );
    } catch (PDOExecption $e) {
        die($e->getMessage());
    }
}

function findOneIntoWorks($id){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `travaux_realises` WHERE `id`= :id');
        $reponse->execute(['id'=>$id]);
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

function findAllIntoWorks(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `travaux_realises`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

//Fonction pour table Divers

function insertIntoVarious($telephone, $mail_perso, $adresse) {
    $db = getPdo();
    try {
        $request = 'INSERT INTO `divers` (`telephone`,`mail_perso`, `adresse`) VALUES (:telephone, :mail_perso, :adresse)';
        $insert = $db->prepare($request);
        $insert->execute(['telephone' => $telephone, 'mail_perso' => $mail_perso, 'adresse' => $adresse]);
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

function deleteIntoVarious($id) {
    $db = getPdo();
    //require_once("index.php");
    $where = ['id' => $id];
    $db->prepare("DELETE FROM `divers` WHERE id=:id")->execute($where);
   
}

function updateVarious($id, $telephone, $mail_perso, $adresse) {
    $db = getPdo();
    try {
        $request = "UPDATE `divers` SET telephone=:telephone, mail_perso=:mail_perso, adresse=:adresse WHERE id=:id";
        $update = $db->prepare($request);
        $update->execute(['telephone' => $telephone, 'mail_perso' => $mail_perso, 'adresse' => $adresse, 'id' => $id] );
    } catch (PDOExecption $e) {
        die($e->getMessage());
    }
}

function findOneIntoVarious($id){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `divers` WHERE `id`= :id');
        $reponse->execute(['id'=>$id]);
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

function findAllIntoVarious(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `divers`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

//Pour s'assurer que les fichiers uploadés correspondent aux attentes
function testImgInput() {

if (isset($_FILES['fichier']) && $_FILES['fichier']['error'] ===0){
    
    $sizeLimit = 1000000; //Un million d'octets = 1Mo
    if ($_FILES['fichier']['size'] <= $sizeLimit){
        $acceptedExtensions = ['jpg','png','gif'];
        $uploadedPathInfo = pathinfo($_FILES['fichier']['name']);
        $uploadedExtension = $uploadedPathInfo['extension'];
        if (in_array($uploadedExtension,$acceptedExtensions)){
            move_uploaded_file($_FILES['fichier']['tmp_name'],'http://localhost/cms_portfolio_2018_2019/portfolio/img/'.$_FILES['fichier']['name']); // http://localhost/cms_portfolio_2018_2019/portfolio/img
            echo 'fichier bien uploadé <br>';
        }
        else{
            echo 'extension '.$uploadedExtension.' pas acceptée <br>';
        }
    }
    else{
        echo 'fichier trop gros<br>';
    }
}
}

//Fonctions pour afficher les images utilisées
function findAllImagesIntoAccueil(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT photo_presentation, id FROM `accueil`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

function findAllImagesIntoServices(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT services_images, id FROM `services_proposes`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

function findAllImagesIntoWorks(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT images_travaux, id FROM `travaux_realises`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}