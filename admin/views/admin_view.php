<?php



function showFirstHtml(){ 
    echo '<html>
    <head>
    <meta charset="utf-8" />
    
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Commande admin Portfolio DELAUNOIS Thibault</title>

	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Roboto:400,700,300" rel="stylesheet" type="text/css">
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
    <!-- Autres modifications graphiques -->
    <link href="http://localhost/cms_portfolio_2018_2019/admin/style.css" rel="stylesheet" />
    </head>
    <body>
        <div class="wrapper">
         <div class="sidebar" data-color="azure" data-image="assets/img/sidebar-5.jpg">

    <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->


    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://localhost/cms_portfolio_2018_2019/admin/" class="simple-text">
                    Commande Administrateur
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="#">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="pe-7s-user"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <i class="pe-7s-note2"></i>
                        <p>Liste des tables</p>
                    </a>
                </li>


                <li>
                    <a href="#">
                        <i class="pe-7s-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li>

            </ul>
    	</div>
    </div>
     <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Liste des tables</a>
                    <a class="navbar-brand" href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=accueil&action=list">Accueil</a>
                    <a class="navbar-brand" href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=services&action=list">Services proposés</a>
                    <a class="navbar-brand" href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=works&action=list">Travaux réalisés</a>
                    <a class="navbar-brand" href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=contact&action=list">Contact</a>
                    <a class="navbar-brand" href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=various&action=list">Divers</a>
                </div>
                
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Page des tables</h4>
                            </div>
                            <div class="content table-responsive table-full-width">                           
    ';
}

//fonctions pour la table accueil
 
function tableListAccueil($accueil){
    $table = '<table>';
    $table .= '<thead>';
    $table .= '<td class="columntitle">id</td>';
    $table .= '<td class="columntitle">Titres accueil</td>';
    $table .= '<td class="columntitle">Textes accueil</td>';
    $table .= '<td class="columntitle">Photo présentation</td>';
    $table .= '<td class="columntitle">actions</td>';
    $table .= '</thead>';
    $table .= '<tbody>';
    foreach ($accueil as $line){
        $table .= '<tr>';
            $table .= '<td>'.$line['id'].'</td>';
            $table .= '<td>'.$line['titre_accueil'].'</td>';
            $table .= '<td>'.$line['texte_accueil'].'</td>';
            $table .= '<td>'.$line['photo_presentation'].'</td>';
            $table .= '<td><a href="?tablename=accueil&amp;action=delete&amp;id='.$line['id'].'">Delete</a></td>';
            $table .= '<td><a href="?tablename=accueil&amp;action=update&amp;id='.$line['id'].'">Update</a></td>';
         
        $table .= '</tr>';
    }
    $table .= '<tbody>';
    $table .= '</table>';

    return $table;
}

//Fonctions pour la table Accueil
function showAccueilForm(){
    return '<form action="" method="post">
        <br>
            <input type="text" id="titre_accueil" name="titre_accueil">
            <label for="titre_accueil">Titre accueil</label>
            <br>
            <br>
            <input type="text" id="texte_accueil" name="texte_accueil">
            <label for="texte_accueil">Texte accueil</label>
            <br>
            <br>
            <input type="varchar(255)" id="photo_presentation" name="photo_presentation">
            <label for="photo_presentation">Photo de présentation</label>
            
            <input type="submit" />
            </form>
';
}

function showDeleteIntoAccueilForm(){
        return '<form action="" method="get">
        <br>
            <input type="int(11)" id="id" name="ID">
            <label for="id">id</label>

            <input type="submit" />
            </form>
';
}

function showEditIntoAccueilForm(){
    return '<form action="" method="post">
        <br>
            <input type="text" id="titre_accueil" name="titre_accueil">
            <label for="titre_accueil">Titre accueil</label>
            <br>
            <br>
            <input type="text" id="texte_accueil" name="texte_accueil">
            <label for="texte_accueil">Texte accueil</label>
            <br>
            <br>
            <input type="varchar(255)" id="photo_presentation" name="photo_presentation">
            <label for="photo_presentation">Photo de présentation</label>
            
            <input type="submit" />
            </form>
';
}

//fonctions pour la table Services proposés

function tableListServices($services){
    $table = '<table>';
    $table .= '<thead>';
    $table .= '<td class="columntitle">id</td>';
    $table .= '<td class="columntitle">Explications Services</td>';
    $table .= '<td class="columntitle">Images Services</td>';
    $table .= '<td class="columntitle">actions</td>';
    $table .= '</thead>';
    $table .= '<tbody>';
    foreach ($services as $line){
        $table .= '<tr>';
            $table .= '<td>'.$line['id'].'</td>';
            $table .= '<td>'.$line['service_expl'].'</td>';
            $table .= '<td>'.$line['services_images'].'</td>';
            $table .= '<td><a href="?tablename=services&amp;action=delete&amp;id='.$line['id'].'">Delete</a></td>';
            $table .= '<td><a href="?tablename=services&amp;action=update&amp;id='.$line['id'].'">Update</a></td>';
         
        $table .= '</tr>';
    }
    $table .= '<tbody>';
    $table .= '</table>';

    return $table;
}

function showServicesForm(){
    return '<form action="" method="post">
        <br>
            <input type="text" id="service_expl" name="service_expl">
            <label for="service_expl">Explications Services Proposés</label>
            <br>
            <br>
            <input type="varchar(255)" id="services_images" name="services_images">
            <label for="services_images">Images Services</label>
            
            <input type="submit" />
            </form>
';
}

function showDeleteIntoServicesForm(){
        return '<form action="" method="get">
        <br>
            <input type="int(11)" id="id" name="ID">
            <label for="id">id</label>

            <input type="submit" />
            </form>
';
}

function showEditIntoServicesForm(){
    return '<form action="" method="post">
        <br>
            <input type="text" id="service_expl" name="service_expl">
            <label for="service_expl">Explications Services Proposés</label>
            <br>
            <br>
            <input type="varchar(255)" id="services_images" name="services_images">
            <label for="services_images">Images Services</label>
            
            <input type="submit" />
            </form>
';
}

//Fonctions pour la table Travaux réalisés

 function tableListWorks($works){
    $table = '<table>';
    $table .= '<thead>';
    $table .= '<td class="columntitle">id</td>';
    $table .= '<td class="columntitle">Titres travaux</td>';
    $table .= '<td class="columntitle">Images travaux</td>';
    $table .= '<td class="columntitle">Vidéos travaux</td>';
    $table .= '<td class="columntitle">actions</td>';
    $table .= '</thead>';
    $table .= '<tbody>';
    foreach ($works as $line){
        $table .= '<tr>';
            $table .= '<td>'.$line['id'].'</td>';
            $table .= '<td>'.$line['titre_travaux'].'</td>';
            $table .= '<td>'.$line['images_travaux'].'</td>';
            $table .= '<td>'.$line['video_travaux'].'</td>';
            $table .= '<td><a href="?tablename=works&amp;action=delete&amp;id='.$line['id'].'">Delete</a></td>';
            $table .= '<td><a href="?tablename=works&amp;action=update&amp;id='.$line['id'].'">Update</a></td>';
         
        $table .= '</tr>';
    }
    $table .= '<tbody>';
    $table .= '</table>';

    return $table;
}

function showWorksForm(){
    return '<form action="" method="post">
        <br>
            <input type="text" id="titre_travaux" name="titre_travaux">
            <label for="titre_travaux">Titre travaux</label>
            <br>
            <br>
            <input type="varchar(255)" id="images_travaux" name="images_travaux">
            <label for="images_travaux">Images travaux</label>
            <br>
            <br>
            <input type="varchar(255)" id="video_travaux" name="video_travaux">
            <label for="video_travaux">Vidéos travaux</label>
            
            <input type="submit" />
            </form>
';
}

function showDeleteIntoWorksForm(){
        return '<form action="" method="get">
        <br>
            <input type="int(11)" id="id" name="ID">
            <label for="id">id</label>

            <input type="submit" />
            </form>
';
}

function showEditIntoWorksForm(){
    return '<form action="" method="post">
        <br>
            <input type="text" id="titre_travaux" name="titre_travaux">
            <label for="titre_travaux">Titre travaux</label>
            <br>
            <br>
            <input type="varchar(255)" id="images_travaux" name="images_travaux">
            <label for="images_travaux">Images travaux</label>
            <br>
            <br>
            <input type="varchar(255)" id="video_travaux" name="video_travaux">
            <label for="video_travaux">Vidéos travaux</label>
            
            <input type="submit" />
            </form>
';
}
 
//Fonctions pour la table Contact
 
function tableListContact($contact){
    $table = '<table>';
    $table .= '<thead>';
    $table .= '<td class="columntitle">id</td>';
    $table .= '<td class="columntitle">Google map</td>';
    $table .= '<td class="columntitle">Nom</td>';
    $table .= '<td class="columntitle">Adresse mail</td>';
    $table .= '<td class="columntitle">Message</td>';
    $table .= '<td class="columntitle">actions</td>';
    $table .= '</thead>';
    $table .= '<tbody>';
    foreach ($contact as $line){
        $table .= '<tr>';
            $table .= '<td>'.$line['id'].'</td>';
            $table .= '<td>'.$line['map'].'</td>';
            $table .= '<td>'.$line['nom'].'</td>';
            $table .= '<td>'.$line['email'].'</td>';
            $table .= '<td>'.$line['message'].'</td>';
            $table .= '<td><a href="?tablename=contact&amp;action=delete&amp;id='.$line['id'].'">Delete</a></td>';
            $table .= '<td><a href="?tablename=contact&amp;action=update&amp;id='.$line['id'].'">Update</a></td>';
         
        $table .= '</tr>';
    }
    $table .= '<tbody>';
    $table .= '</table>';

    return $table;
}

function showContactForm(){
    return '<form action="" method="post">
        <br>
            <input type="varchar(255)" id="map" name="map">
            <label for="map">Carte Google</label>
            <br>
            <br>
            <input type="text" id="nom" name="nom">
            <label for="nom">Nom</label>
            <br>
            <br>
            <input type="varchar(255)" id="email" name="email">
            <label for="email">Adresse mail</label>
            <br>
            <br>
            <input type="text" id="message" name="message">
            <label for="message">Message</label>
            
            <input type="submit" />
            </form>
';
}

function showDeleteIntoContactForm(){
        return '<form action="" method="get">
        <br>
            <input type="int(11)" id="id" name="ID">
            <label for="id">id</label>

            <input type="submit" />
            </form>
';
}

function showEditIntoContactForm(){
    return '<form action="" method="post">
        <br>
            <input type="varchar(255)" id="map" name="map">
            <label for="map">Carte Google</label>
            <br>
            <br>
            <input type="text" id="nom" name="nom">
            <label for="nom">Nom</label>
            <br>
            <br>
            <input type="varchar(255)" id="email" name="email">
            <label for="email">Adresse mail</label>
            <br>
            <br>
            <input type="text" id="message" name="message">
            <label for="message">Message</label>
            
            <input type="submit" />
            </form>
';
}

//Fonctions pour la table Divers

function tableListVarious($various){
    $table = '<table>';
    $table .= '<thead>';
    $table .= '<td class="columntitle">id</td>';
    $table .= '<td class="columntitle">Numéro de téléphone</td>';
    $table .= '<td class="columntitle">Adresse mail personnelle</td>';
    $table .= '<td class="columntitle">Adresse physique</td>';
    $table .= '<td class="columntitle">actions</td>';
    $table .= '</thead>';
    $table .= '<tbody>';
    foreach ($various as $line){
        $table .= '<tr>';
            $table .= '<td>'.$line['id'].'</td>';
            $table .= '<td>'.$line['telephone'].'</td>';
            $table .= '<td>'.$line['mail_perso'].'</td>';
            $table .= '<td>'.$line['adresse'].'</td>';
            $table .= '<td><a href="?tablename=various&amp;action=delete&amp;id='.$line['id'].'">Delete</a></td>';
            $table .= '<td><a href="?tablename=various&amp;action=update&amp;id='.$line['id'].'">Update</a></td>';
         
        $table .= '</tr>';
    }
    $table .= '<tbody>';
    $table .= '</table>';

    return $table;
}

function showVariousForm(){
    return '<form action="" method="post">
        <br>
            <input type="varchar(255)" id="telephone" name="telephone">
            <label for="telephone">Numéro de téléphone</label>
            <br>
            <br>
            <input type="varchar(255)" id="mail_perso" name="mail_perso">
            <label for="mail_perso">Adresse mail personnelle</label>
            <br>
            <br>
            <input type="varchar(255)" id="adresse" name="adresse">
            <label for="adresse">Adresse physique</label>
            
            <input type="submit" />
            </form>
';
}

function showDeleteVariousForm(){
        return '<form action="" method="get">
        <br>
            <input type="int(11)" id="id" name="ID">
            <label for="id">id</label>

            <input type="submit" />
            </form>
';
}

function showEditIntoVariousForm(){
    return '<form action="" method="post">
        <br>
            <input type="varchar(255)" id="telephone" name="telephone">
            <label for="telephone">Numéro de téléphone</label>
            <br>
            <br>
            <input type="varchar(255)" id="mail_perso" name="mail_perso">
            <label for="mail_perso">Adresse mail personnelle</label>
            <br>
            <br>
            <input type="varchar(255)" id="adresse" name="adresse">
            <label for="adresse">Adresse physique</label>
            
            <input type="submit" />
            </form>
';
}

//Formulaire pour ajouter images
function showFileInputForm() {
    return '<br/><br/>
        <form action="" method="post" enctype="multipart/form-data">
    <input type="file" name="fichier">
    <input type="submit" >
    </form><br/><br/>';
}

//Fonctions pour voir les images utilisées

//function showAccueilImages($accueilimg){
//    if ($accueilimg['photo_presentation'] !== null){
//    echo '<img src="'.$accueilimg['photo_presentation'].'" alt="image_accueil_'.$accueilimg['id'].'" /></div>';
//    }
//}
//
//function showServicesImages($servicesimg){
//    if ($servicesimg['services_images'] !== null){
//    echo '<img src="'.$servicesimg['services_images'].'" alt="image_service_'.$servicesimg['id'].'" /></div>';
//    }
//}
//
//function showWorksImages($worksimg){
//    if ($worksimg['images_travaux'] !== null){
//    echo '<img src="'.$worksimg['images_travaux'].'" alt="image_travaux_'.$worksimg['id'].'" /></div>';
//    }
//}