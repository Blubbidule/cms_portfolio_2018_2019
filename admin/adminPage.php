<?php
session_start();
include 'model/admin_model.php';
include 'views/admin_view.php';


if(isset($_SESSION['token']) || isset($_COOKIE['token']) ) {
    
       
    $tablename = filter_input(INPUT_GET, 'tablename');
    $action = filter_input(INPUT_GET, 'action');
    $id = filter_input(INPUT_GET, 'id');
    $tableandaction = array($tablename,$action);

    //Affichage de la table appropriée
    switch($tableandaction){
       

        //Pour la table accueil
        case array('accueil','delete'):
            
                deleteIntoAccueil($_GET['id']);
                redirect('accueil','list');
                showFirstHtml();
            break;
        case array('accueil','update'):
           if (isformAccueilSubmitted() == true){
                updateAccueil($_GET['id'],$_POST['titre_accueil'],$_POST['texte_accueil'],$_POST['photo_presentation']);
                redirect('accueil','list');
                showFirstHtml();
            }
            else{
                $content = showAccueilForm();
                showFirstHtml();
            }
            break;
        case array('accueil','insert'):
            if (isformAccueilSubmitted() == true){
                insertIntoAccueil($_POST['titre_accueil'],$_POST['texte_accueil'],$_POST['photo_presentation']);
                redirect('accueil','list');
                showFirstHtml();
            }
            else{
                $content = showAccueilForm();
                showFirstHtml();
            }

            break;
        case array('accueil','list'):
        default:
            $message = findAllIntoAccueil();
            $content = tableListAccueil($message);
            showFirstHtml();
            break;

    //Pour la table Services proposés
        case array('services','delete'):
                deleteIntoServices($_GET['id']);
                redirect('services','list');
                showFirstHtml();
            break;
        case array('services','update'):
           if (isformServicesSubmitted() == true){
                updateServices($_GET['id'],$_POST['service_expl'],$_POST['services_images']);
                redirect('services','list');
                showFirstHtml();
            }
            else{
                $content = showServicesForm();
                showFirstHtml();
            }
            break;
        case array('services','insert'):
            if (isformServicesSubmitted() == true){
                insertIntoServices($_POST['service_expl'],$_POST['services_images']);
                redirect('services','list');
                showFirstHtml();
            }
            else{
                $content = showServicesForm();
                showFirstHtml();
            }

            break;
        case array('services','list'):
            $message = findAllIntoServices();
            $content = tableListServices($message);
            showFirstHtml();
            break;

    //Pour la table Travaux réalisés
        case array('works','delete'):
                deleteIntoWorks($_GET['id']);
                redirect('works','list');
                showFirstHtml();
            break;
        case array('works','update'):
           if (isformWorksSubmitted() == true){
                updateServices($_GET['id'],$_POST['titre_travaux'],$_POST['images_travaux'],$_POST['video_travaux']);
                redirect('works','list');
                showFirstHtml();
            }
            else{
                $content = showWorksForm();
                showFirstHtml();
            }
            break;
        case array('works','insert'):
            if (isformWorksSubmitted() == true){
                insertIntoWorks($_POST['titre_travaux'],$_POST['images_travaux'],$_POST['video_travaux']);
                redirect('works','list');
                showFirstHtml();
            }
            else{
                $content = showWorksForm();
                showFirstHtml();
            }

            break;
        case array('works','list'):
            $message = findAllIntoWorks();
            $content = tableListWorks($message);
            showFirstHtml();
            break;

    //Pour la table Contact
       case array('contact','delete'):
                deleteIntoContact($_GET['id']);
                redirect('contact','list');
                showFirstHtml();
            break;
        case array('contact','update'):
           if (isformContactSubmitted() == true){
                updateContact($_GET['id'],$_POST['map'],$_POST['nom'],$_POST['email'],$_POST['message']);
                redirect('contact','list');
                showFirstHtml();
            }
            else{
                $content = showContactForm();
                showFirstHtml();
            }
            break;
        case array('contact','insert'):
            if (isformContactSubmitted() == true){
                insertIntoContact($_POST['map'],$_POST['nom'],$_POST['email'],$_POST['message']);
                redirect('contact','list');
                showFirstHtml();
            }
            else{
                $content = showContactForm();
                showFirstHtml();
            }

            break;
        case array('contact','list'):
            $message = findAllIntoContact();
            $content = tableListContact($message);
            showFirstHtml();
            break;

    //Pour la table Divers
        case array('various','delete'):
                deleteIntoVarious($_GET['id']);
                redirect('various','list');
                showFirstHtml();
            break;
        case array('various','update'):
           if (isformVariousSubmitted() == true){
                updateVarious($_GET['id'],$_POST['telephone'],$_POST['mail_perso'],$_POST['adresse']);
                redirect('various','list');
                showFirstHtml();
            }
            else{
                $content = showVariousForm();
            }
            break;
        case array('various','insert'):
            if (isformVariousSubmitted() == true){
                insertIntoVarious($_POST['telephone'],$_POST['mail_perso'],$_POST['adresse']);
                redirect('various','list');
                showFirstHtml();
            }
            else{
                $content = showVariousForm();
            }

            break;
        case array('various','list'):
            $message = findAllIntoVarious();
            $content = tableListVarious($message);
            showFirstHtml();
            break; 
    }

    echo $content;

    //Formulaire pour les différentes tables
    function isformAccueilSubmitted(){
        if ((isset($_POST['titre_accueil']) && isset($_POST['texte_accueil'])) || isset($_POST['photo_presentation'])){
            $_POST['titre_accueil'] = htmlspecialchars ($_POST['titre_accueil']);
                $_POST['texte_accueil'] = htmlspecialchars ($_POST['texte_accueil']);
                $_POST['photo_presentation'] = htmlspecialchars ($_POST['photo_presentation']);
            return true;
        }
        else{
            return false;
        }
    }

    function isformServicesSubmitted(){
        if ((isset($_POST['service_expl'])) && isset($_POST['services_images'])){
            $_POST['service_expl'] = htmlspecialchars ($_POST['service_expl']);
            $_POST['services_images'] = htmlspecialchars ($_POST['services_images']);
            return true;
        }
        else{
            return false;
        }
    }

    function isformWorksSubmitted(){
        if ((isset($_POST['titre_travaux'])) &&(isset($_POST['images_travaux']) || isset($_POST['video_travaux']))){
            $_POST['titre_travaux'] = htmlspecialchars ($_POST['titre_travaux']);
            $_POST['images_travaux'] = htmlspecialchars ($_POST['images_travaux']);
            $_POST['video_travaux'] = htmlspecialchars ($_POST['video_travaux']);
            return true;
        }
        else{
            return false;
        }
    }

    function isformContactSubmitted(){
        if ((isset($_POST['map'])) || ((isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['message'])) && (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['email'])))){
            $_POST['map'] = htmlspecialchars ($_POST['map']);
            $_POST['nom'] = htmlspecialchars ($_POST['nom']);
            $_POST['email'] = htmlspecialchars ($_POST['email']);
            $_POST['message'] = htmlspecialchars ($_POST['message']);

            
                return true;
            
            

            } else {
                return false;
        }
    }

    function isformVariousSubmitted(){
        if (isset($_POST['telephone']) && isset($_POST['mail_perso']) && isset($_POST['adresse'])){
            $_POST['telephone'] = htmlspecialchars ($_POST['telephone']);
            $_POST['mail_perso'] = htmlspecialchars ($_POST['mail_perso']);
            $_POST['adresse'] = htmlspecialchars ($_POST['adresse']);

             if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['mail_perso']) && preg_match("#(04[6-9][0-9]((([ ./]?[0-9]{2}){3})|(([ ./]?[0-9]{3}){2})))#", $_POST['telephone']))
            {
                return true;
            }
                else
            {
             echo 'L\'adresse ' . $_POST['mail_perso'] . ' et/ou le numéro de téléphone ' . $_POST['telephone'] . ' n\'est pas valide';
            }
        }
        else{
            return false;
        }
    }

    //Redirection après delete, update ou insert
    function redirect($tablename,$action){
        header("Location: ?tablename=".$tablename."&action=".$action);
    }

    //Liens pour retour et insert pour les différents tables
    switch ($tablename) {
        case 'accueil':
           echo '                      </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=accueil&action=insert">ajouter un titre et un texte ou une photo</a><br/>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=accueil&action=list">retour</a>'.showFileInputForm();
            testImgInput().'
            </div>
            </body>
            </html>';
            break;

        case 'services':
           echo '                      </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=services&action=insert">ajouter une description des services</a><br/>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=services&action=list">retour</a>'.showFileInputForm();
            testImgInput().'
            </div>
            </body>
            </html>';
            break;

        case 'works':
           echo '                      </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=works&action=insert">ajouter un travail</a><br/>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=works&action=list">retour</a>'.showFileInputForm();
            testImgInput().'
            </div>
            </body>
            </html>';
            break;

        case 'contact':
           echo '                      </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=contact&action=insert">ajouter des données de contact</a><br/>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=contact&action=list">retour</a>
            </div>
            </body>
            </html>';
            break;

        case 'various':
           echo '                      </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=various&action=insert">ajouter des données diverses</a><br/>
                <a href="http://localhost/cms_portfolio_2018_2019/admin/adminPage.php?tablename=various&action=list">retour</a>
            </div>
            </body>
            </html>';
            break;
    }

    } else {
        header('Location: index.php');
    }
    
        