<?php

include 'view/portfolio_view.php';
include 'model/portfolio_model.php';

$page = filter_input(INPUT_GET, 'page');

switch ($page){
    case'accueil' :
    default:
        showAccueilPage();
        break;
    
    case'services' :
        showServicesPage();
        break;
    
    case'works' :
        showWorksPage();
        break;
    
    case'contact' :
        showContactPage();
        break;
    
}

//Ajout footer
echo '	 
        </div>
        <footer>
	   <div class="col col-7-12"><p> </p></div>
	   
	   <div class="col col-4-12">
	     <div class="copyright">
	       &copy; 2019 Delaunois Thibault <a href="http://www.seraingsup.be/">IPEPS Seraing Sup</a>
	     </div><!--droit d\'auteur du site-->
	   </div>
	   
	   <div class="col col-1-12 last"><p> </p></div>
	   <div class="clear"></div>
	   
	 </footer>
         
   
   </body>
   
 </html>';