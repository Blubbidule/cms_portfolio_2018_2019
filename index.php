<?php


include 'model/portfolio_model.php';
include 'views/portfolio_view.php';
$page = filter_input(INPUT_GET, 'page');

switch ($page){
    case 'accueil':
    default:
        showStartHtml();
        ShowLinksAccueil();
        $titlesandtexts = getContentFromAccueil() ;
        $content = showAccueilPageContent ($titlesandtexts);
        echo $content;
        showEndHtml();
        
    break;

    case 'services':
        showStartHtml();
        ShowLinksServices();
        $servtext = getTextContentFromServices();
        $serviceimg = getImgContentFromServices();
        $content = showServicesPageText($servtext);
        $content .= showServicesPageImg($serviceimg);
        echo $content;
        showEndHtml();
    break;

    case 'works':
        showStartHtml();
        ShowLinksWorks();
        $titleandimg = getContentFromWorks();
        $content = showWorksContent($titleandimg);
        echo $content;
        showEndHtml();
    break;

    case 'contact':
        showStartHtml();
        ShowLinksContact();
        $yourpersonnaldata = getPersonnalData();
        $thegooglemap = getGoogleMap();
        $content = showContactPersonalData($yourpersonnaldata);
        $content .= showContactForm();
        $content .= showContactMap($thegooglemap);
        echo $content;
        showEndHtml();
    break;
}
