<?php

function getPdo() {
    try {
        $db = new PDO('mysql:host=localhost;dbname=cms_portfolio;charset=utf8', 'root', 'root');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        die('connection error : ' . $e->getMessage());
    }

    return $db;
}

//Pour afficher le contenu (texte) de la page Accueil
function getContentFromAccueil(){
        $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT titre_accueil,texte_accueil FROM `accueil`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}



//pour afficher le texte de la page service

function getTextContentFromServices(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT service_expl FROM `services_proposes` WHERE `id`= 1');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

//Pour afficher les images de la page services
function getImgContentFromServices(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT services_images, id FROM `services_proposes`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}


//Seulement la vidéo
function getmovieContentFromWorks(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT titre_travaux, video_travaux, id FROM `travaux_realises`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

//Les images et leur titre des travaux
function getContentFromWorks(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT titre_travaux, images_travaux, id FROM `travaux_realises`');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetchAll();
}

//Les données personnelles de la page Contact
function getPersonnalData(){
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT * FROM `divers` WHERE `id`= 1 ');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

//La carte google map
function getGoogleMap() {
    $db = getPdo();
    try{
        $reponse = $db->prepare('SELECT map FROM `contact` WHERE `id`= 1');
        $reponse->execute();
    } catch (PDOException $ex) {
        die($ex->getMessage());
    }
    return $reponse->fetch();
}

//Pour permettre l'envoi de données depuis le formulaire de la page Contact
function insertFromFormIntoContact( $name, $email, $message) {
    $db = getPdo();
    try {
        $request = 'INSERT INTO `contact` (`nom`,`email`,`message`) VALUES (:name, :email, :message)';
        $insert = $db->prepare($request);
        $insert->execute(['name' => $name, 'email'=>$email, 'message'=>$message]);
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

//Pour valider le formulaire
function isFormFromContactSubmitted(){
    if ((isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['message'])) && (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['email']))){
        $_POST['nom'] = htmlspecialchars ($_POST['nom']);
        $_POST['email'] = htmlspecialchars ($_POST['email']);
        $_POST['message'] = htmlspecialchars ($_POST['message']);
        return true;
    }
    else{
        return false;
    }
}


if (isFormFromContactSubmitted() == true){
            insertFromFormIntoContact($_POST['nom'], $_POST['email'], $_POST['message']);
            echo '<p>Message envoyé</p>';
            redirect('contact');
        }else {
            echo '<p>adresse mail incorrect</p>';
        }
        
function redirect($page){
        header("Location: ?page=".$page);
    }
